# Backloggery #

Emulates a library of items by double hierarchy lists, using JavaFX for GUI. The app saves the list to two text files (one for backup) for persistent data.
Each item contains fields for common game entry values (as I intended to use the list for tracking a game library).

![Screen Shot 2015-03-05 at 1.12.02 PM.png](https://bitbucket.org/repo/xjkoqy/images/3408551146-Screen%20Shot%202015-03-05%20at%201.12.02%20PM.png)