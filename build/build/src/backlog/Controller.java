package backlog;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;

import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;

import javafx.scene.control.ListView;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;


public class Controller {
	//TODO plan out how i would write this whole app "properly", as detailed as i dare to
	//deploy properly using the bookmark
	
	ObservableList<GameModel> currentList = FXCollections.observableArrayList();
	ObservableList<ListModel> lists = FXCollections.observableArrayList();
	GameModel currentGame;
	ListModel currentListModel;
	GameModel temp;
	boolean newList = false;
	MenuItem tempMenu;
	int p;
	String listTemptName = "";
	FileWriter w;
	FileReader r;
	BufferedWriter bw;
	BufferedReader br;
	ListModel blargh;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button addGame;
    
    @FXML
    private TextField listName;

    @FXML
    private Button addList;

    @FXML
    private CheckBox finished;

    @FXML
    private TextArea gameNotes;

    @FXML
    private ListView<GameModel> gamesList;

    @FXML
    private ListView<ListModel> listsList;

    @FXML
    private MenuButton moveGame;

    @FXML
    private TextField platform;

    @FXML
    private TextField playTime;

    @FXML
    private TextField playthroughs;

    @FXML
    private Button removeGame;

    @FXML
    private Button removeList;

    @FXML
    private Button save;

    @FXML
    private TextField series;

    @FXML
    private TextField title;

    @FXML
    private TextField yearsPlayed;


    @FXML
    void addGamePressed(ActionEvent event) {
    	if (!gamesList.getSelectionModel().isEmpty())
    		currentList.add(new GameModel());
    }

    @FXML
    void addListPressed(ActionEvent event) {
    	ListModel l = new ListModel("wishlist");
    	blargh = l;
    	lists.add(l);
    	currentList = l.games;
    	
    	refreshMenu();
    	
    	
    }
    
    void refreshMenu() {
    	MenuItem item;
    	String s;
    	while (!moveGame.getItems().isEmpty())
    		moveGame.getItems().remove(0);
    	for (int i = 0; i<lists.size(); i++) {
    		s = lists.get(i).toString();
    		item = new MenuItem(s);
    		
    		item.setOnAction(new EventHandler<ActionEvent>() {
    			public void handle(ActionEvent e) {
    				GameModel g = gamesList.getSelectionModel().selectedItemProperty().getValue();
    				currentList.remove(g);
    				MenuItem i = (MenuItem) e.getSource();
    				String s = i.getText();
					ListModel l = getList(s);
					l.games.add(g);
    			}
    		});
    		
    		moveGame.getItems().add(item);
    	}

    	
    	
    }
    
    ListModel getList(String name) {
    	for (ListModel l : lists) {
    		if (l.name.equals(name))
    			return l;
    	}
    	throw new RuntimeException();
    }

    @FXML
    void removeGamePressed(ActionEvent event) {
    	GameModel g = gamesList.getSelectionModel().selectedItemProperty().getValue();
    	currentList.remove(g);
    }

    @FXML
    void removeListPressed(ActionEvent event) {
    	ListModel l = listsList.getSelectionModel().selectedItemProperty().getValue();
    	if (!lists.isEmpty())
    		lists.remove(l);
    	refreshMenu();
    }
    
    void openWrite() {
    	try
		{
			w = new FileWriter("Save.txt");
			bw = new BufferedWriter(w);
		} catch (IOException e)
		{
			e.printStackTrace();
		}
    }
    void openRead() {
    	try
		{
			r = new FileReader("Save.txt");
			br = new BufferedReader(r);	
		} catch (IOException e)
		{
			e.printStackTrace();
		}
    }

    @FXML
    void savePressed(ActionEvent event) {
    	//format:
    	//list ;; game // game var // game var // .. ;; game // .. :: list .. 
    	openWrite();
    	save();
    	try
		{
			bw.close();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
    	
    	
    }
    
    void saveFields() {
    	listsList.getSelectionModel().getSelectedItem().setName(listName.getText());
    	
    	gamesList.getSelectionModel().getSelectedItem().
    				setTitle(title.getText()).setPlatform(platform.getText()).
    				setArea(gameNotes.getText()).setTimePlayed(playTime.getText()).
    				setPlaythroughs(playthroughs.getText()).setYearsPlayed(yearsPlayed.getText()).
    				setSeries(series.getText()).setFinished(finished.isSelected());
    	refresh();
    }
    
    void save() {
    	saveFields();
    	try
		{
    		for (ListModel l : lists)
    		{
    			bw.write(l.toString());
    			bw.write(";;");
    			for (GameModel g : l.games)
    			{
    				bw.write(g.title);
    				bw.write("//");
    				bw.write(g.platform);
    				bw.write("//");
    				bw.write(g.series);
    				bw.write("//");
    				if (g.finished)
    					bw.write("yes");
    				else bw.write("no");
    				bw.write("//");
    				bw.write(g.yearsPlayed);
    				bw.write("//");
    				bw.write(g.timePlayed);
    				bw.write("//");
    				bw.write(g.playthroughs);
    				bw.write("//");
    				bw.write(g.freeText);
    				bw.write(";;");
    			}
    			bw.write("::");
    		}
		} catch (IOException e)
		{
			e.printStackTrace();
		}
    	
    	refresh();
    }
    
    public void showGameDetails(ObservableList<GameModel> game) {
    	if (game != null) {
        	currentList = game;
        	gamesList.setItems(game);
        	gamesList.getSelectionModel().select(0);
        	
    	}

    }
    
    
    public void showList(ListModel list) {
    	currentList = list.games;
    	showGameDetails(list.games);
    	
    }
    
    public void showGame(GameModel game) { 
    	GameModel f = gamesList.getSelectionModel().getSelectedItem();
    	if (f != null) {
    		title.setText(f.title);
        	yearsPlayed.setText(f.yearsPlayed);
        	series.setText(f.series);
        	playthroughs.setText(f.playthroughs);
        	platform.setText(f.platform);
        	playTime.setText(f.timePlayed);
        	finished.setSelected(f.finished);
        	gameNotes.setText(f.freeText);
    	}
    		
    	
    }
    
    void loadFile() {
    	openRead();
    	String s;
    	ObservableList<GameModel> readGames = FXCollections.observableArrayList();
    	ListModel tempList; GameModel tempGame = new GameModel();
    	try {

    		if ((s = br.readLine()) != null)
    		{
    			String[] games;
    			String[] listo = s.split("::");
    			String[] gamo;
    			for (String saa : listo) 
    			{
    				games = saa.split(";;");
    				tempList = new ListModel(games[0]);
    				for (int i=1; i<games.length; i++)
    				{
        				gamo = games[i].split("//");
        				tempGame = new GameModel();
        				tempGame.setTitle(gamo[0]).setPlatform(gamo[1]).setSeries(gamo[2]).
        							setYearsPlayed(gamo[4]).setTimePlayed(gamo[5]).
        							setPlaythroughs(gamo[6]).setArea(gamo[7]);
        				if (gamo[3].equals("yes"))
        					tempGame.setFinished(true);
        				else tempGame.setFinished(false);
        				
        				readGames.add(tempGame);
    				}
    				tempList.games = readGames;
    				lists.add(tempList);
    				readGames = FXCollections.observableArrayList();
    			}
    		}
    	} catch (IOException ex) {
    		ex.printStackTrace();
    	}
    	
    	
    	try
		{
			br.close();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
    }
    
    static void copy() {
			try
			{
				File f = new File("Save.txt");
				File s = new File("SaveBackup.txt");
				s.delete();
				//FileOutputStream f = new FileOutputStream("SaveBackup.txt");
				//FileInputStream i = new FileInputStream("Save.txt");
				java.nio.file.Files.copy(f.toPath(), s.toPath());
				//f.close();
				//i.close();
			} catch (Exception e)
			{
			}

    }

    @FXML
    void initialize() {
    	
    	copy();
    	
    	listsList.setEditable(true);
    	
		listsList.setItems(lists);

    	gamesList.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<GameModel>() {
			@Override
			public void changed(ObservableValue<? extends GameModel> arg0,
					GameModel oldValue, GameModel newValue)
			{
				if (!newList);
				{
					temp = gamesList.getSelectionModel().getSelectedItem();
				showGame(temp);
				}
				//gamesList.getSelectionModel().select(newValue);
				
			}
    	});
    	listsList.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<ListModel>() {
    		@Override
    		public void changed(ObservableValue<? extends ListModel> a, ListModel oldValue, ListModel newValue)
    		{
    	    	listName.setText(listsList.getSelectionModel().getSelectedItem().toString());
    			//temp = listsList.getSelectionModel().selectedItemProperty().getValue().games
    			//		.get(0);
    			showList(newValue);
				//listsList.getSelectionModel().select(newValue);
    		}
    	});
    	
    	
    	loadFile();
    	refreshMenu();
    	
    	
    	
        assert addGame != null : "fx:id=\"addGame\" was not injected: check your FXML file 'Layout.fxml'.";
        assert addList != null : "fx:id=\"addList\" was not injected: check your FXML file 'Layout.fxml'.";
        assert finished != null : "fx:id=\"finished\" was not injected: check your FXML file 'Layout.fxml'.";
        assert gameNotes != null : "fx:id=\"gameNotes\" was not injected: check your FXML file 'Layout.fxml'.";
        assert gamesList != null : "fx:id=\"gamesList\" was not injected: check your FXML file 'Layout.fxml'.";
        assert listsList != null : "fx:id=\"listsList\" was not injected: check your FXML file 'Layout.fxml'.";
        assert moveGame != null : "fx:id=\"moveGame\" was not injected: check your FXML file 'Layout.fxml'.";
        assert platform != null : "fx:id=\"platform\" was not injected: check your FXML file 'Layout.fxml'.";
        assert playTime != null : "fx:id=\"playTime\" was not injected: check your FXML file 'Layout.fxml'.";
        assert playthroughs != null : "fx:id=\"playthroughs\" was not injected: check your FXML file 'Layout.fxml'.";
        assert removeGame != null : "fx:id=\"removeGame\" was not injected: check your FXML file 'Layout.fxml'.";
        assert removeList != null : "fx:id=\"removeList\" was not injected: check your FXML file 'Layout.fxml'.";
        assert save != null : "fx:id=\"save\" was not injected: check your FXML file 'Layout.fxml'.";
        assert series != null : "fx:id=\"series\" was not injected: check your FXML file 'Layout.fxml'.";
        assert title != null : "fx:id=\"title\" was not injected: check your FXML file 'Layout.fxml'.";
        assert yearsPlayed != null : "fx:id=\"yearsPlayed\" was not injected: check your FXML file 'Layout.fxml'.";


    }
    
    
    void refresh() {
    	listsList.setItems(listsList.getItems());
    	
    	refreshMenu();
    }

}
