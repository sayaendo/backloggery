package backlog;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Test
{
	
	FileWriter w;
	FileReader r;
	BufferedWriter bw;
	BufferedReader br;

	public static void main(String[] args)
	{
		new Test().start();

	}
	
	void start() {
		try
		{
			w = new FileWriter("Save.txt");
			bw = new BufferedWriter(w);
			bw.write("...");
			bw.close();
			
			r = new FileReader("Save.txt");
			br = new BufferedReader(r);
			System.out.print(br.readLine());
			br.close();
		} catch (IOException e)
		{
			e.printStackTrace();
		}	
	}

}
