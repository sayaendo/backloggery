package backlog;

import java.io.IOException;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class MainApp extends Application {
	
	public MainApp() {
		
	}
	
	Stage primaryStage;
	AnchorPane rootLayout;

	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
	    try {
	    // Load the root layout from the fxml file
	    	FXMLLoader loader = new FXMLLoader(this.getClass().getResource("Layout.fxml"));
	        rootLayout = (AnchorPane) loader.load();
	        Scene scene = new Scene(rootLayout);
	        primaryStage.setScene(scene);
	        primaryStage.show();
	        
	        Controller controller = loader.getController();
	        
	    } catch (IOException e) {
	        // Exception gets thrown if the fxml file could not be loaded
	        e.printStackTrace();
	    }
		
	}

	public static void main(String[] args) {
		launch(args);
	}
}
