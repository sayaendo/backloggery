package backlog;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class ListModel implements Comparable<ListModel>
{
	
	ObservableList<GameModel> games = FXCollections.observableArrayList();
	
	String name;
	
	public ListModel(String n) {
		name = n;
		games.add(new GameModel());
	}
	
	public void addGame(GameModel game) {
		games.add(game);
	}
	
	public void setName(String s) {
		this.name = s;
	}
	
	@Override
	public String toString() {
		return name;
	}
	
	@Override
	public int compareTo(ListModel l) {
		return (l.name.compareTo(name));
	}

}
