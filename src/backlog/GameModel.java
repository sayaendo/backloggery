package backlog;

public class GameModel implements Comparable<GameModel>
{
	
	String title = "-";
	String platform = "-";
	String series = "-";
	boolean finished;
	String yearsPlayed = "-";
	String timePlayed = "-";
	String playthroughs = "-";
	String freeText = "-";
	
	public GameModel setTitle(String s) {
		title = s;
		return this;
	}
	
	public GameModel setArea(String s) {
		freeText = s;
		return this;
	}
	
	public GameModel setPlatform(String s) {
		platform = s;
		return this;
	}
	
	public GameModel setSeries(String s) {
		series = s;
		return this;
	}
	
	public GameModel setFinished(boolean b) {
		finished = b;
		return this;
	}
	
	public GameModel setYearsPlayed(String s) {
		yearsPlayed = s;
		return this;
	}
	
	public GameModel setTimePlayed(String s) {
		timePlayed = s;
		return this;
	}
	
	public GameModel setPlaythroughs(String s) {
		playthroughs = s;
		return this;
	}
	
	@Override
	public String toString() {
		String s = series + ": " + title + " (" + platform + ")";
		return s;
	}

	
	@Override
	public int compareTo(GameModel l) {
		return (l.title.compareTo(title));
	}

}
